from random import randint
player_name = input("Hi! What is your name?")
# guess_number = 1
for guess_number in range(1,6):
    month_number = randint(1,12)
    year_number = randint(1990,2022)
    print("Guess", guess_number, ":", player_name, "were you", "born on", month_number, "/", year_number, "?")
    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
         print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")

